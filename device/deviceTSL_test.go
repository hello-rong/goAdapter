package device

import "testing"

func TestDeviceTSLAdd(t *testing.T) {

	tsl1 := &DeviceTSLTemplate{
		Name: "HL7031",
	}
	DeviceTSLMap = append(DeviceTSLMap, tsl1)

	tsl := &DeviceTSLTemplate{
		Name: "HL7031",
	}

	status, err := DeviceTSLAdd(tsl)
	t.Logf("status %v,err %v", status, err)
}

func TestDeviceTSLDelete(t *testing.T) {

	tsl := &DeviceTSLTemplate{
		Name: "HL7031",
	}
	DeviceTSLMap = append(DeviceTSLMap, tsl)

	tsl1 := &DeviceTSLTemplate{
		Name: "HL7032",
	}
	DeviceTSLMap = append(DeviceTSLMap, tsl1)

	status, err := DeviceTSLDelete("HL7032")
	t.Logf("status %v,err %v", status, err)
}

func TestDeviceTSLModify(t *testing.T) {

	tsl := &DeviceTSLTemplate{
		Name:    "HL7031",
		Explain: "1111",
	}
	DeviceTSLMap = append(DeviceTSLMap, tsl)

	tsl1 := DeviceTSLTemplate{
		Name:    "HL7031",
		Explain: "2222",
	}

	status, err := DeviceTSLModify(&tsl1)
	t.Logf("status %v,err %v", status, err)

	t.Logf("DeviceTSLMap %v", DeviceTSLMap)
}

func TestDeviceTSLTemplate_DeviceTSLPropertiesAdd(t *testing.T) {

	property := DeviceTSLPropertyTemplate{
		Name:       "nUa",
		Explain:    "A相电压",
		AccessMode: TSLAccessModeRead,
		Type:       PropertyTypeInt32,
	}

	tsl := &DeviceTSLTemplate{
		Name:       "HL7031",
		Explain:    "1111",
		Properties: make([]DeviceTSLPropertyTemplate, 0),
	}
	status, err := tsl.DeviceTSLPropertiesAdd(property)
	if err != nil {
		t.Logf("status %v,err %v", status, err)
	}

	DeviceTSLMap = append(DeviceTSLMap, tsl)
	t.Logf("DeviceTSLMap %v", DeviceTSLMap)

}

func TestDeviceTSLTemplate_DeviceTSLPropertiesDelete(t *testing.T) {

	property := DeviceTSLPropertyTemplate{
		Name:       "nUa",
		Explain:    "A相电压",
		AccessMode: TSLAccessModeRead,
		Type:       PropertyTypeInt32,
	}

	tsl := DeviceTSLTemplate{
		Name:       "HL7031",
		Explain:    "1111",
		Properties: make([]DeviceTSLPropertyTemplate, 0),
	}
	status, err := tsl.DeviceTSLPropertiesAdd(property)
	if err != nil {
		t.Logf("status %v,err %v", status, err)
	}

	DeviceTSLMap = append(DeviceTSLMap, &tsl)
	for _, v := range DeviceTSLMap {
		t.Logf("DeviceTSLMap0 %v", *v)
	}

	names := make([]string, 0)
	names = append(names, "nUa")
	status, err = tsl.DeviceTSLPropertiesDelete(names)
	if err != nil {
		t.Logf("status %v,err %v", status, err)
	}

	for _, v := range DeviceTSLMap {
		t.Logf("DeviceTSLMap0 %v", *v)
	}
}
