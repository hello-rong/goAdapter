package mqttAliyun

import (
	"encoding/json"
	"goAdapter/device"
	"goAdapter/setting"
	"strings"
)

func (r *ReportServiceParamAliyunTemplate) ProcessInvokeThingsService() {

	for {
		select {
		case reqFrame := <-r.InvokeThingsServiceRequestFrameChan:
			{
				setting.ZAPS.Debugf("reqFrame %v", reqFrame)
				methodParam := strings.Split(reqFrame.Method, ".")
				if len(methodParam) != 3 {
					continue
				}
				if reqFrame.Params["Name"] == nil {
					continue
				}
				name := reqFrame.Params["Name"].(string)

				index := -1
				for k, v := range r.NodeList {
					if v.Name == name {
						index = k
					}
				}
				if index == -1 {
					continue
				}
				device.CollectInterfaceMap.Lock.Lock()
				coll, ok := device.CollectInterfaceMap.Coll[r.NodeList[index].CollInterfaceName]
				device.CollectInterfaceMap.Lock.Unlock()
				if !ok {
					continue
				}
				for _, d := range coll.DeviceNodeMap {
					if name == d.Name {
						cmd := device.CommunicationCmdTemplate{}
						cmd.CollInterfaceName = coll.CollInterfaceName
						cmd.DeviceName = name
						cmd.FunName = methodParam[2]
						paramStr, _ := json.Marshal(reqFrame.Params)
						cmd.FunPara = string(paramStr)

						ack := MQTTAliyunInvokeThingsServiceAckTemplate{}
						ackData := coll.CommQueueManage.CommunicationManageAddEmergency(cmd)
						if ackData.Status {
							ack.ID = reqFrame.ID
							ack.Code = 200
							MQTTAliyunThingServiceAck(r.GWParam.MQTTClient, r.GWParam, ack, cmd.FunName)
						} else {
							ack.ID = reqFrame.ID
							ack.Code = 1000
							MQTTAliyunThingServiceAck(r.GWParam.MQTTClient, r.GWParam, ack, cmd.FunName)
						}
					}
				}
			}
		}
	}
}
