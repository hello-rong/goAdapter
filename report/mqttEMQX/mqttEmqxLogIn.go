package mqttEmqx

import (
	"encoding/json"
	"goAdapter/setting"
	"strconv"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"
)

type MQTTNodeLoginParamTemplate struct {
	ClientID  string `json:"clientID"`
	Timestamp int64  `json:"timestamp"`
}

type MQTTNodeLoginTemplate struct {
	ID      string                       `json:"id"`
	Version string                       `json:"version"`
	Params  []MQTTNodeLoginParamTemplate `json:"params"`
}

type MQTTEmqxLogInDataTemplate struct {
	ProductKey string `json:"productKey,omitempty"`
	DeviceName string `json:"deviceName,omitempty"`
}

type MQTTEmqxLogInAckTemplate struct {
	ID      string                      `json:"id"`
	Code    int32                       `json:"code"`
	Message string                      `json:"message"`
	Data    []MQTTEmqxLogInDataTemplate `json:"data,omitempty"`
}

var MsgID int = 0

func MQTTEmqxOnConnectHandler(client MQTT.Client) {
	setting.ZAPS.Info("MQTTEmqx 链接成功")
	for _, v := range ReportServiceParamListEmqx.ServiceList {
		if v.GWParam.MQTTClient == client {
			subTopic := ""
			//子设备上线回应
			subTopic = "/sys/thing/node/login/post_reply/" + v.GWParam.Param.ClientID
			MQTTEmqxSubscribeTopic(client, subTopic)

			//子设备下线回应
			subTopic = "/sys/thing/node/logout/post_reply/" + v.GWParam.Param.ClientID
			MQTTEmqxSubscribeTopic(client, subTopic)

			//子设备属性设置上报回应
			subTopic = "/sys/thing/node/property/post_reply/" + v.GWParam.Param.ClientID
			MQTTEmqxSubscribeTopic(client, subTopic)

			//订阅子设备属性下发请求
			subTopic = "/sys/thing/node/property/set/" + v.GWParam.Param.ClientID
			MQTTEmqxSubscribeTopic(client, subTopic)

			//订阅子设备属性查询请求
			subTopic = "/sys/thing/node/property/get/" + v.GWParam.Param.ClientID
			MQTTEmqxSubscribeTopic(client, subTopic)

			//订阅子设备服务调用请求
			subTopic = "/sys/thing/node/service/invoke/" + v.GWParam.Param.ClientID
			MQTTEmqxSubscribeTopic(client, subTopic)

			//订阅获取子设备设备状态请求
			subTopic = "/sys/thing/node/status/get/" + v.GWParam.Param.ClientID
			MQTTEmqxSubscribeTopic(client, subTopic)
		}
	}
}

func MQTTEmqxConnectionLostHandler(client MQTT.Client, err error) {
	setting.ZAPS.Debug("MQTTEmqx 链接断开")
}

func MQTTEmqxReconnectHandler(client MQTT.Client, opt *MQTT.ClientOptions) {
	setting.ZAPS.Debug("MQTTEmqx 链接重新链接")
}

func MQTTEmqxGWLogin(param ReportServiceGWParamEmqxTemplate, publishHandler MQTT.MessageHandler) (bool, MQTT.Client) {

	opts := MQTT.NewClientOptions().AddBroker(param.IP + ":" + param.Port)

	opts.SetClientID(param.Param.ClientID)
	opts.SetUsername(param.Param.UserName)
	opts.SetPassword(param.Param.Password)
	opts.SetKeepAlive(30 * time.Second)
	opts.SetDefaultPublishHandler(publishHandler)
	opts.SetAutoReconnect(true)
	//opts.SetConnectRetry(false)
	opts.SetConnectTimeout(2 * time.Second)
	opts.SetOnConnectHandler(MQTTEmqxOnConnectHandler)
	opts.SetConnectionLostHandler(MQTTEmqxConnectionLostHandler)
	opts.SetReconnectingHandler(MQTTEmqxReconnectHandler)

	// create and start a client using the above ClientOptions
	mqttClient := MQTT.NewClient(opts)
	if token := mqttClient.Connect(); token.Wait() && token.Error() != nil {
		setting.ZAPS.Debugf("上报服务[%s]链接EMQX Broker失败 %v", param.ServiceName, token.Error())
		return false, nil
	}
	setting.ZAPS.Infof("上报服务[%s]链接EMQX Broker成功", param.ServiceName)

	subTopic := ""
	//子设备上线回应
	subTopic = "/sys/thing/node/login/post_reply/" + param.Param.ClientID
	MQTTEmqxSubscribeTopic(mqttClient, subTopic)

	//子设备下线回应
	subTopic = "/sys/thing/node/logout/post_reply/" + param.Param.ClientID
	MQTTEmqxSubscribeTopic(mqttClient, subTopic)

	//属性设置上报回应
	subTopic = "/sys/thing/node/property/post_reply/" + param.Param.ClientID
	MQTTEmqxSubscribeTopic(mqttClient, subTopic)

	//订阅属性下发请求
	subTopic = "/sys/thing/node/property/set/" + param.Param.ClientID
	MQTTEmqxSubscribeTopic(mqttClient, subTopic)

	//订阅属性查询请求
	subTopic = "/sys/thing/node/property/get/" + param.Param.ClientID
	MQTTEmqxSubscribeTopic(mqttClient, subTopic)

	//订阅服务调用请求
	subTopic = "/sys/thing/node/service/invoke/" + param.Param.ClientID
	MQTTEmqxSubscribeTopic(mqttClient, subTopic)

	//订阅获取设备状态请求
	subTopic = "/sys/thing/node/status/get/" + param.Param.ClientID
	MQTTEmqxSubscribeTopic(mqttClient, subTopic)

	return true, mqttClient

}

func MQTTEmqxSubscribeTopic(client MQTT.Client, topic string) {

	if token := client.Subscribe(topic, 0, nil); token.Wait() && token.Error() != nil {
		setting.ZAPS.Warnf("EMQX上报服务订阅主题%s失败 %v", topic, token.Error())
		return
	}
	setting.ZAPS.Infof("EMQX上报服务订阅主题%s成功", topic)
}

func (r *ReportServiceParamEmqxTemplate) GWLogin() bool {
	status := false
	status, r.GWParam.MQTTClient = MQTTEmqxGWLogin(r.GWParam, ReceiveMessageHandler)
	if status == true {
		r.GWParam.ReportStatus = "onLine"
	}

	return status
}

func MQTTEmqxNodeLoginIn(param ReportServiceGWParamEmqxTemplate, nodeMap []string) int {

	nodeLogin := MQTTNodeLoginTemplate{
		ID:      strconv.Itoa(MsgID),
		Version: "V1.0",
	}
	MsgID++

	for _, v := range nodeMap {
		nodeLoginParam := MQTTNodeLoginParamTemplate{
			ClientID:  v,
			Timestamp: time.Now().Unix(),
		}
		nodeLogin.Params = append(nodeLogin.Params, nodeLoginParam)
	}

	//批量注册
	loginTopic := "/sys/thing/node/login/post/" + param.Param.ClientID

	sJson, _ := json.Marshal(nodeLogin)
	if len(nodeLogin.Params) > 0 {
		setting.ZAPS.Infof("上报服务[%s]发布节点上线消息主题%s", param.ServiceName, loginTopic)
		setting.ZAPS.Debugf("上报服务[%s]发布节点上线消息内容%s", param.ServiceName, sJson)
		if param.MQTTClient != nil {
			token := param.MQTTClient.Publish(loginTopic, 0, false, sJson)
			token.Wait()
		}
	}

	return MsgID
}

func (r *ReportServiceParamEmqxTemplate) NodeLogIn(name []string) bool {

	nodeMap := make([]string, 0)
	status := false

	setting.ZAPS.Debugf("上报服务[%s]节点%s上线", r.GWParam.ServiceName, name)
	for _, d := range name {
		for _, v := range r.NodeList {
			if d == v.Name {
				nodeMap = append(nodeMap, v.Param.ClientID)

				MQTTEmqxNodeLoginIn(r.GWParam, nodeMap)
				select {
				case frame := <-r.ReceiveLogInAckFrameChan:
					{
						if frame.Code == 200 {
							status = true
						}
					}
				case <-time.After(time.Millisecond * 2000):
					{
						status = false
					}
				}
			}
		}
	}

	return status
}
